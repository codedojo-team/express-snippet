const express = require('express');
const crypto = require('crypto');
const moment = require('moment');
const { highlight } = require('highlight.js');
require('moment/locale/ru');

moment.locale('ru');

let snippets = require('./snippets.json');

const server = express();

server.set('view engine', 'pug');

server.locals.moment = moment;

server.use(express.static('public'));
server.use('/lib', express.static('node_modules'));
server.use(express.urlencoded({ extended: false }));

server.get('/', (req, res) => {
    res.render('index', {
        title: 'Express Snippet',
        snippets
    });
});

server.get('/create', (req, res) => {
    res.render('form', {
        snippet: {}
    });
});

server.post('/create', (req, res) => {
    const snippet = {
        id: crypto.randomBytes(8).toString('hex'),
        createdAt: Date.now(),
        ...req.body
    };
    
    snippets.push(snippet);

    res.redirect('/');
});

server.get('/search', (req, res) => {
    if (!req.query.query) return res.redirect('/');

    const query = req.query.query.toLowerCase();

    const foundSnippets = snippets.filter(s =>
        s.filename.toLowerCase().includes(query) ||
        s.description.toLowerCase().includes(query) ||
        s.language.toLowerCase().includes(query)
    );

    res.render('search', {
        title: 'Результаты поиска',
        query: req.query.query,
        snippets: foundSnippets
    });
});

server.get('/:snippetId', (req, res) => {
    const snippet = snippets.find(s => s.id == req.params.snippetId);

    if (!snippet) {
        return res.redirect('/');
    }

    res.render('view', {
        snippet,
        highlight
    });
});

server.get('/:snippetId/edit', (req, res,) => {
    const snippet = snippets.find(s => s.id == req.params.snippetId);

    if (!snippet) {
        return res.redirect('/');
    }

    res.render('form', {
        snippet
    });
});

server.post('/:snippetId/edit', (req, res) => {
    const snippet = snippets.find(s => s.id == req.params.snippetId);

    if (!snippet) {
        return res.redirect('/');
    }

    snippets = snippets.map(s =>
        s.id != snippet.id ? s : {
            ...s,
            ...req.body,
            updatedAt: Date.now()
        }
    );

    res.redirect(`/${snippet.id}`);
});

server.post('/:snippetId/delete', (req, res) => {
    const snippet = snippets.find(s => s.id == req.params.snippetId);

    if (!snippet) {
        return res.redirect('/');
    }

    snippets = snippets.filter(s => s.id !== req.params.snippetId);

    res.redirect('/');
});

server.listen(3000, () => console.log('http://localhost:3000'));